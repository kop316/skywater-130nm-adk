# Setup

This repository converts the SkyWater 130 nm PDK into the format required by mflowgen for running a digital flow. mflowgen requires the following files in `view_standard` directory (for now, we will perform the setup for only regular VT cells, in typical corner). Once complete, your folder should have the following:
```
stdcells.mwlib/ #Directory
adk.tcl
calibre-drc-block.rule
calibre.layerprops
calibre-lvs.rule
rtk-stream-out.map
rtk-tech.lef
rtk-tech.tf
rtk-typical.captable
stdcells.cdl
stdcells.db
stdcells.gds
stdcells.lib
stdcells.lef
stdcells.spi
stdcells.v
```

# Steps
1. Edit `SKYWATER130_HOME` in `skywater_path.py` to point to the top folder of the skywater-pdk repository.
2. We are manually fixing the PR mentioned here: (https://github.com/google/skywater-pdk-libs-sky130_fd_sc_hd/pull/5 ). (The first half is fixed at the end). `python3 generate_rtk_lef.py` copies technology lef file into `rtk-tech.lef`. Edit this file to add these lines after the last metal layer, otherwise LEF generation in Innovus complains. Mentioned here: https://github.com/google/skywater-pdk-libs-sky130_fd_sc_hd/pull/5.
```
LAYER OVERLAP
  TYPE OVERLAP ;
END OVERLAP

```
Manually add the `licon` layer after `pwell` layer definition in `rtk-tech.lef`.  This is really hacky, and should be fixed properly (https://github.com/google/skywater-pdk-libs-sky130_fd_sc_hd/pull/5 ).
```
LAYER poly
	TYPE MASTERSLICE ;
END poly

LAYER licon1
  TYPE CUT ;

  WIDTH 0.17 ;                # Licon 1
  SPACING 0.17 ;              # Licon 2
  ENCLOSURE BELOW 0 0 ;       # Licon 4
  ENCLOSURE ABOVE 0.08 0.08 ; # Poly / Met1 4 / Met1 5
END licon1
```

3. Go into the `generate_captable` folder, and follow the steps in its `README.md`. Generating captables takes several hours. This creates `rtk-typical.captable`. Only the last step depends on this being finished, so you can continue to work on everything else in parallel.
4. Run `python3 generate_lib.py` to copy typical lib file from the PDK. Then go into the `generate_lib` folder, and follow the steps in its `README.md`, since the copied lib file doesn't work out of the box with commercial tools.
5. Run `python3 generate_sc_lef.py` to generate `stdcells.lef` by concatenating the lef files for all cells in `SKYWATER130_HOME + '/libraries/sky130_fd_sc_hd/latest/cells/'` into a single file.
6. Run `python3 generate_spi.py` to generate `stdcells.spi` by concatenating the spice files for all cells.
7. Run `python3 generate_cdl.py` to generate `stdcells.cdl` by concatenating the cdl files for all cells.
8. Run `python3 generate_verilog.py` to generate `stdcells.v` by concatenating the Verilog files for all cells.
9. Go into the `generate_db` folder, and follow the steps in its `README.md` to generate `stdcells.db`.
10. Go into the `generate_milkyway` folder, and follow the steps in its `README.md`. This generates `stdcells.mwlib` and `rtk-tech.tf`.
11. Run `python3 generate_gds.py` to copy all standard cell GDS's into a `stdcellsgds` directory. This takes a while (minutes) to complete. Then run the following command to create a merged GDS file.
```
module load calibre
/cad/mentor/2019.1/aoi_cal_2019.1_18.11/bin/calibredrv -a layout filemerge -indir stdcellsgds -out view-standard/stdcells.gds
```
12. `rtk-stream-out.map` is copied from https://foss-eda-tools.googlesource.com/skywater-pdk/libs/sky130_osu_sc/+/refs/heads/master/flow/pnr/streamOut.map. No action on your part is required.
13. `adk.tcl` is handwritten looking at the lef and lib files. No action on your part is required.
14. The final files relate to Calibre and Magic to Check DRC and LVS:

14a. The final three calibre files are not available yet. We need to create some scripts that generate these files from the technology information in the PDK.

14b. As an alternative, we are using magic to check DRCs, and extract a SPICE netlist from the layout and netgen for LVS. Magic needs a `.magicrc` file to be in the folder from which magic is invoked, and a `.tech` file. Netgen also needs a setup file. We will get these files from the `open_pdks` repo. Follow the steps in the [Digital Flow](https://git.ece.cmu.edu/ctalbot/stanford-ee272-skywater) to compile `open_pdk`, then copy these files:
```
cp ${OPEN_PDK_INSTALL_FOLER}/share/pdk/sky130A/libs.tech/magic/sky130A.magicrc view-standard/magicrc
cp ${OPEN_PDK_INSTALL_FOLER}/share/pdk/sky130A/libs.tech/magic/sky130A.tcl view-standard
cp ${OPEN_PDK_INSTALL_FOLER}/share/pdk/sky130A/libs.tech/magic/sky130A.tech view-standard
cp ${OPEN_PDK_INSTALL_FOLER}/share/pdk/sky130A/libs.tech/netgen/sky130A_setup.tcl view-standard/netgen_setup.tcl
```
There may be a way to skip the `share/pdk` directories when generating with `open_pdks`. We cannot simply move the directory since the path is coded in the files. We should fix this next time.
